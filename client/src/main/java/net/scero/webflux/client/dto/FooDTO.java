package net.scero.webflux.client.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/*
 *
 *
 * @author joseluis.nogueira on 05/10/2019
 */
@Data
@AllArgsConstructor
public class FooDTO {
  private int id;
  private String name;
}
