package net.scero.webflux.client.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

/*
 *
 *
 * @author joseluis.nogueira on 04/10/2019
 */
@RestController
@RequestMapping(value = "/client")
public class ClientController {
  private final static String URL = "http://localhost:8080";

  @GetMapping(value = "flux", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Flux<String> retrieveFlux(){
    WebClient webClient = WebClient.create(URL);
    return webClient.get().uri("/server/flux").retrieve().bodyToFlux(String.class).map(e -> "Remote result: " + e);
  }

  @GetMapping(value = "list")
  public CompletableFuture<List<String>> retrieveList() {
    WebClient webClient = WebClient.create(URL);

    Mono<List<String>> results = webClient.get().uri("/server/flux").retrieve().bodyToFlux(String.class).collect(Collectors.toList());
    return results.toFuture();
  }

  @GetMapping(value = "string-timeout")
  public CompletableFuture<String> retriveStringWithTimeout(@RequestParam(value = "timeout", required = true) int timeout){
    StringBuilder sb = new StringBuilder();
    CompletableFuture<String> completableFuture = new CompletableFuture<>();

    WebClient webClient = WebClient.create(URL);
    webClient.get().uri("/server/flux").retrieve().bodyToFlux(String.class).
      take(Duration.ofSeconds(timeout)).
      doOnTerminate(() -> completableFuture.complete(sb.toString())).
      subscribe(e -> sb.append(e));

    return completableFuture;
  }

  @GetMapping(value = "string-timeout-fail")
  public CompletableFuture<String> retriveStringWithTimeoutAndFail(@RequestParam(value = "timeout", required = true) int timeout){
    StringBuilder sb = new StringBuilder();
    CompletableFuture<String> completableFuture = new CompletableFuture<>();

    WebClient webClient = WebClient.create(URL);
    webClient.get().uri("/server/flux").retrieve().bodyToFlux(String.class).
      take(Duration.ofSeconds(timeout)).
      timeout(Duration.ofSeconds(timeout)).
      doOnError(e -> completableFuture.complete(e instanceof TimeoutException ? "Timeout" : "Error: " + e.getMessage())).
      doOnTerminate(() -> completableFuture.complete(sb.toString())).
      subscribe(e -> sb.append(e));

    return completableFuture;
  }

  @GetMapping(value = "merge", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Flux<String> mergeResults(){
    WebClient webClient1 = WebClient.create(URL);
    WebClient webClient2 = WebClient.create(URL);

    Flux<String> flux1 = webClient1.get().uri("/server/flux").retrieve().bodyToFlux(String.class);
    Flux<String> flux2 = webClient2.get().uri("/server/flux-json").retrieve().bodyToFlux(String.class);

    return Flux.merge(flux1, flux2);
  }
}
