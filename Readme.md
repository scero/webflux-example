==========================
SERVER
==========================

curl -X GET "http://localhost:8080/server/mono"
    Retrieve a Mono result 

curl -X GET "http://localhost:8080/server/flux"
    Retrieve a Flux Result
    
curl -X GET "http://localhost:8080/server/fibonacci"
curl -X GET "http://localhost:8080/server/fibonacci?limit=20"
    Retrieve a Flux with fibonacci
    
curl -X GET "http://localhost:8080/server/list"
    Classical endpoint 

==========================
CLIENT
==========================
    
curl http://localhost:8090/client/flux
    This endpoint retrieve a flux from /server/flux
    Modify each item
    Relay a flux with items  
    
curl -X GET "http://localhost:8090/client/string-timeout?timeout=10"
    This endpoint retrieve a flux from /server/flux
    At 10 seconds, retransmit all the information you have been able to receive
    
curl -X GET "http://localhost:8090/client/string-timeout-fail?timeout=3"
    This endpoint retrieve a flux from /server/flux
    If you arrive at the timeout seconds, response error
    
curl -X GET "http://localhost:8090/client/merge"
    This endpoint retrive data from endpoints /server/flux and /server/flux-json
    Relay a flux with merge results

