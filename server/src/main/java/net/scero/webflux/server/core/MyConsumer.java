package net.scero.webflux.server.core;

import reactor.core.publisher.SynchronousSink;

import java.util.LinkedList;
import java.util.function.Consumer;

/*
 * For generate a Flux dynamic
 *
 * @author joseluis.nogueira on 07/10/2019
 */
public class MyConsumer implements Consumer<SynchronousSink<String>> {
  private final LinkedList<String> buffer;

  public MyConsumer() {
    buffer = new LinkedList<>();
    for(int i = 0; i < 20; i++) {
      buffer.offer("Hello world !!!");
      buffer.offer("Winter is comming");
      buffer.offer("And now ?");
    }
  }


  @Override
  public void accept(SynchronousSink<String> fluxSink) {
    try {
      Thread.sleep(100l);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(buffer.size());
    fluxSink.next(buffer.poll());
    if (buffer.isEmpty()) {
      fluxSink.complete();
    }
  }
}
