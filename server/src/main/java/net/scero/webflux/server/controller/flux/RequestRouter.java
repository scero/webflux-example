package net.scero.webflux.server.controller.flux;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

/*
 *
 *
 * @author joseluis.nogueira on 05/10/2019
 */
@Configuration
public class RequestRouter {
  @Autowired
  private RequestHandler requestHandler;

  @Bean
  RouterFunction<?> routes(RequestHandler requestHandler) {
    return RouterFunctions
            .route(RequestPredicates.GET("/webflux/flux"),
                    requestHandler::retrieveFlux);
  }
}
