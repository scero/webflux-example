package net.scero.webflux.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication
// @EnableWebFlux
public class WebfluxServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebfluxServerApplication.class, args);
	}

}
