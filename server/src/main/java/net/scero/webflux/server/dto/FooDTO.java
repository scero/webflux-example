package net.scero.webflux.server.dto;

import lombok.Data;

/*
 *
 *
 * @author joseluis.nogueira on 30/10/2019
 */
@Data
public class FooDTO {
  private String value;
}
