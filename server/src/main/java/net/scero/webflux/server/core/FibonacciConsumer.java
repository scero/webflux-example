package net.scero.webflux.server.core;

import reactor.core.publisher.SynchronousSink;

import java.util.function.Consumer;

/*
 * Sample fibonacci consumer
 *
 * @author joseluis.nogueira on 29/10/2019
 */
public class FibonacciConsumer implements Consumer<SynchronousSink<Long>> {
  private final long limit;
  private long position;

  public FibonacciConsumer(long limit){
    this.limit = limit;
    this.position = 0;
  }

  @Override
  public void accept(SynchronousSink<Long> fluxSink) {
    long value = fib(position);
    if (value < limit){
      position++;
      fluxSink.next(value);
    } else {
      fluxSink.complete();
    }
  }

  private long fib(long n){
    if (n < 2) {
      return n;
    } else {
      return fib(n-1) + fib(n-2);
    }
  }
}
