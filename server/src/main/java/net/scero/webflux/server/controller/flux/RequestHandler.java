package net.scero.webflux.server.controller.flux;

import lombok.extern.slf4j.Slf4j;
import net.scero.webflux.server.core.MyConsumer;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SynchronousSink;

import java.util.LinkedList;
import java.util.function.Consumer;

/*
 *
 *
 * @author joseluis.nogueira on 05/10/2019
 */
@Component
@Slf4j
public class RequestHandler {
  public Mono<ServerResponse> retrieveFlux(ServerRequest request) {
    Flux<String> flux = Flux.generate(new MyConsumer());
    return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(flux, String.class);
  }
}
