package net.scero.webflux.server.controller;

import net.scero.webflux.server.core.FibonacciConsumer;
import net.scero.webflux.server.dto.FooDTO;
import net.scero.webflux.server.core.MyConsumer;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

/*
 *
 *
 * @author joseluis.nogueira on 04/10/2019
 */
@RestController
@RequestMapping(value = "/server")
public class ExampleRestController {
  @GetMapping(path = "/mono", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Mono<String> retrieveMono() {
    return Mono.just("Hello world !!!!");
  }

  @GetMapping(path = "/flux", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Flux<String> retrieveFlux() {
    Flux<String> flux = Flux.generate(new MyConsumer());
    return flux;
  }

  @GetMapping(path = "/flux-json", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  public Flux<FooDTO> retrieveFluxJson(){
    Flux<FooDTO> flux = Flux.generate(new MyConsumer()).map(e -> {
      FooDTO fooDTO = new FooDTO();
      fooDTO.setValue(e);
      return fooDTO;
    });
    return flux;
  }

  @GetMapping(path = "/fibonacci", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Flux<Long> retrieveFibonacci(@RequestParam(value = "limit", required = false) Integer limit) {
    return Flux.generate(new FibonacciConsumer(limit != null ? limit.intValue() : 2000));
  }

  @GetMapping(path = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<String> retrieveList(){
    List<String> list = new ArrayList<>();
    for(int i = 0; i < 200; i++){
      list.add("Hello world !!!");
      list.add("Winter is comming");
      list.add("And now ?");
    }
    return list;
  }
}
