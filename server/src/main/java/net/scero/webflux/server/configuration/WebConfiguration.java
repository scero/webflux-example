package net.scero.webflux.server.configuration;

import org.springframework.context.annotation.Configuration;

/*
 * Only for Spring MVC
 *
 * @author joseluis.nogueira on 04/10/2019
 */
@Configuration
public class WebConfiguration {}
/* implements WebMvcConfigurer {
  public static final int DEFAULT_POOL_SIZE = 10;
  public static final int DEFAULT_QUEUE_SIZE = 100;

  @Bean
  public Filter shallowEtagHeaderFilter() {
    return new ShallowEtagHeaderFilter();
  }

  @Override
  public void configureAsyncSupport(AsyncSupportConfigurer configurer){
    final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(DEFAULT_POOL_SIZE);
    executor.setMaxPoolSize(DEFAULT_POOL_SIZE);
    executor.setQueueCapacity(DEFAULT_QUEUE_SIZE);
    executor.setThreadNamePrefix("AsyncTaskExecutor-");
    executor.initialize();
    configurer.setTaskExecutor(executor);
  }
}
*/
